function B= move_me(A)
n=input('n=');
B = n*ones(size(A));
m=A~=n; 
B(6-sum(m):end) = A(m);
end
